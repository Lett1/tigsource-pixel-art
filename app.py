import requests
from bs4 import BeautifulSoup
import concurrent.futures
import itertools
import json
import os.path


URL = "https://forums.tigsource.com/index.php?topic=167"
POSTS_PER_PAGE = 20

def check_if_exists(url):
    try:
        r = requests.head(url)
        return r.status_code == requests.codes.ok
    except:
        return False


def extract_images(url):

    filename = "data/" + url.split("?")[-1] + ".json"

    if os.path.isfile(filename):
        return

    print(f"Fetching images from {url}")

    r = requests.get(url)

    soup = BeautifulSoup(r.text, features="html.parser")

    results = []

    index = 1

    for post in soup.find_all("div", class_="post"):
        index += 1

        for quote in soup.select("div.quote"):
            quote.decompose()

        images = post.find_all("img")
        images = [i for i in images if "forums.tigsource.com/Smileys" not in i["src"]]
        images = [img["src"] for img in images if check_if_exists(img["src"])]

        if images:
            link = post.parent.select("div[id^=subject_]")[0].a["href"]
            try:
                profile_url = post.parent.parent.select('a[href*="action=profile"]')[0]["href"]
                profile_name = post.parent.parent.select('a[href*="action=profile"]')[0].text
            except:
                profile_url = ""
                profile_name = post.parent.parent.select("tr:nth-child(1) > td:nth-child(1) > b")[0].text

            results.append((images, link, profile_name, profile_url))

    with open(filename, "w") as f:
        json.dump(results, f)

def get_page_count(url):
    r = requests.get(url)

    soup = BeautifulSoup(r.text, features="html.parser")

    return int(soup.select("a.navPages")[-1].text)

if __name__ == "__main__":
    pagecount = get_page_count(URL)

    executor = concurrent.futures.ThreadPoolExecutor(8)

    pages = (URL + f".{x*POSTS_PER_PAGE}" for x in range(pagecount))

    
    #pages = ["https://forums.tigsource.com/index.php?topic=167.180"]

    results = executor.map(extract_images, pages)

    #print(extract_images("https://forums.tigsource.com/index.php?topic=167.32360"))