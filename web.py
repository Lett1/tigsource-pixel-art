import json
import glob
import itertools
from jinja2 import Environment, FileSystemLoader, select_autoescape

env = Environment(
    loader=FileSystemLoader('templates/'),
    autoescape=select_autoescape(['html', 'xml'])
)

DATA_PATH = "data/*.json"

def load_json():
    data = []

    files = sorted(glob.glob(DATA_PATH), key=lambda x: int(x.split(".")[1]))

    for file in files:
        with open(file, "r") as f:
            for entry in json.load(f):
                data.append({"images": entry[0], "link": entry[1], "profile_name": entry[2], "profile_url": entry[3]})

    return data

def slice_sequence(seq, rowlen):
    for start in range(0, len(seq), rowlen):
        yield seq[start:start + rowlen]


if __name__ == "__main__":
    data = list(load_json())
    pages = list(slice_sequence(data, 100))

    print(len(pages))

    page_template = env.get_template("page.html")
    index_template = env.get_template("index.html")

    with open("public/index.html", "w", encoding="utf-8") as f:
        f.write(index_template.render(totalpages=len(pages)))

    for pagenum, page in enumerate(pages):
        with open(f"public/page{pagenum+1}.html", "w", encoding="utf-8") as f:
            f.write(page_template.render(data=page, pagenum=pagenum+1,totalpages=len(pages)))